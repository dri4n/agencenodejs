window.$ = window.jQuery = require('jquery');
require('moment');
require('select2');
require('angular');
require('angular-chart.js');

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};

const AgenceApp = angular.module('AgenceApp', ['chart.js']);
require('./controllers/consultorCtrl.js');
require('./controllers/chartLineCtrl.js');
require('./controllers/chartPieCtrl.js');