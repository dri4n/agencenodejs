angular.module('AgenceApp').controller('ConsultoresCtrl', ['$rootScope','$scope', function($rootScope,$scope) {


	$scope.consultorInvoices = undefined;
	$scope.changes 			 = false;
	$scope.consultores    	 = [];

	$scope.filter = {
		users 	 : [],
		dateFrom : null,
		dateTo	 : null
	}

	var dataRangeOptions = {};
	var select2Options 	 = {
        theme: 'bootstrap',
        placeholder: "Seleccionar consultores",
    };

    // seccion del mal junto a jquery
    $('.input-daterange').datepicker(dataRangeOptions);
    $('#selector').select2(select2Options);
    // fin de sección del mal 

	function getConsultores(){
		axios.get('consultores').then((response) => {
			if(response.data){
				$scope.consultores = response.data;
				$scope.$apply();
			}
		});
	};

	function getInvoices(){

		let query 		= '';
		let users 		= $scope.filter.users;
		let userslength = users.length;

		for (var i = 0; i < userslength ; i++) {
			query += 'users='+users[i];
			if(i !== userslength - 1)
				query += '&';
		}

		axios.get('invoices?'+query,{
			params : $scope.filter
		}).then((response) => {
			if(response.data){
				$scope.consultorInvoices = [].concat(response.data);
				if(!$scope.consultorInvoices)
					$scope.consultorInvoices = [];

				applyChanges();
			}
		});
	}

	function applyChanges(){
		$scope.$apply(() => {
			notifyChanges();
		});
	}

	function notifyChanges(){
		$scope.$broadcast('InvoiceChanges',$scope.consultorInvoices);
	}


	// seccion del mal junto a jquery
	function onSelectAll(){
        var selectedItems = [];
        if($(this).is(':checked'))
            $("#selector option").each(function() {
                selectedItems.push( $(this).val() );
            });
        $("#selector").val(selectedItems).trigger("change");
        $scope.changes = true;
	}

	function onSelect(evt){
		var $element = $(evt.params.data.element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
	}

	$("#selectAll").click(onSelectAll);
    $("select").on("select2:select",onSelect);
    // fin de seccion del mal 

	getConsultores();


	// implementación publica.
	$scope.getInvoices = getInvoices;

}]);