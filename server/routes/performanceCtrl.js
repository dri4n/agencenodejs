var express = require('express');
var router = express.Router();
var db 	= require('./../db');
var moment = require('moment');

module.exports = function (app) {
    app.use('/', router);
};


router.get('/', function(req, res, next) {
   res.render('performance');
});

router.get('/consultores',function(req,res,nex){
	  var rawQuery = `
	  	SELECT usuario.co_usuario as 'Id',usuario.no_usuario as 'Username' FROM cao_usuario as usuario 
		INNER JOIN permissao_sistema as permisos ON usuario.co_usuario = permisos.co_usuario 
		WHERE permisos.co_tipo_usuario in (0,1,2) and permisos.co_sistema = 1 and permisos.in_ativo = 'S'
	  `;

	  db.query(rawQuery,function(err,results){
	  	 var consultores = results.map(function(consultor){
	  	 	return consultor;
	  	 });
	  	 res.json(consultores);
	  });
});

router.get('/invoices',function(req,res,nex){
	var rawQuery = `

		SELECT 
		
		t.usuario as 'usuario',
		concat(t.month,'/',t.year) as 'InvoiceDate',
		t.valor as 'Price',
		t.salario as 'NetConsultorPrice',
		t.comission as 'Commission',
		(t.valor - ( t.comission + t.salario)) as 'Gains'
		FROM 
		(
			SELECT
			YEAR(factura.data_emissao) as 'year',
			MONTH(factura.data_emissao) as 'month',
			usuario.no_usuario as 'usuario',
			sum(
                CASE 
                WHEN factura.total_imp_inc > 0 THEN round(factura.valor - ((factura.valor / factura.total_imp_inc / 100 )),2) 
                ELSE round(factura.valor,2)
                END
            ) as 'valor',
            sum(factura.total_imp_inc) as 'totalTax',
			sum(factura.valor - ((factura.valor * factura.total_imp_inc ) * factura.comissao_cn)) as 'comission',
			max(salario.brut_salario) as 'salario'

			FROM cao_fatura factura 
			INNER JOIN cao_os ordenServicio on ordenServicio.co_os = factura.co_os
			INNER JOIN cao_usuario usuario on ordenServicio.co_usuario = usuario.co_usuario
			INNER JOIN cao_salario salario on salario.co_usuario = usuario.co_usuario

			WHERE usuario.co_usuario in (?) and factura.data_emissao >= ? and  factura.data_emissao <= ?

			GROUP BY YEAR(factura.data_emissao), MONTH(factura.data_emissao),usuario.co_usuario
			ORDER BY usuario.co_usuario ASC, YEAR(factura.data_emissao) ASC, MONTH(factura.data_emissao) ASC
		) AS t

	`;

	var query = req.query;
	var dateFrom = moment(new Date(query.dateFrom)).format('YY-MM-DD');
	var dateTo = moment(new Date(query.dateTo)).format('YY-MM-DD');

	db.query(rawQuery,[query.users,dateFrom,dateTo],function(err,results){

		var consultorVM = [];
		var consultorAux;

		results.forEach((invoice) => {
			
			var consultor = invoice.usuario;
			if(!consultorAux || (consultorAux && consultor !== consultorAux) ){
				consultorVM[consultorVM.length] = {
					Consultor : {
						Username : invoice.usuario
					},
					Invoices   : [],
					TotalCommision :0,
					TotalConsultorPrice:0,
					TotalGains:0,
					TotalPrice:0
				};
			}

			consultor = consultorVM[consultorVM.length-1];
			consultor.Invoices.push(invoice);
			consultorAux = invoice.usuario;
		});


		consultorVM.forEach((consultor,key) => {
			consultor.Invoices.forEach((invoice) => {
				consultorVM[key].TotalCommision += invoice.Commission;
				consultorVM[key].TotalConsultorPrice += invoice.NetConsultorPrice;
				consultorVM[key].TotalGains += invoice.Gains;
				consultorVM[key].TotalPrice += invoice.Price;
			});
		});

		res.json(consultorVM);
		
	});

});

