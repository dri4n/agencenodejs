var express = require('express');
var expressLayouts = require('express-ejs-layouts');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var glob = require('glob');
var mysql = require('mysql');
var app = express();



// view engine setup
app.set('view engine', 'ejs');
app.use(expressLayouts);

// especificamos el layout
app.set("layout extractScripts", true)
app.set('layout', path.join(__dirname, 'views/layouts/layout'));

// registramos las vistas
app.set('views', [path.join(__dirname, 'app/resources/js/views'),path.join(__dirname, 'views')]);


// registramos las rutas

require('./server/routes/router')(app);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/public',express.static(path.join(__dirname, 'app/public')));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
